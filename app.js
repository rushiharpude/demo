const express = require('express');
const app = express();

app.use(express.urlencoded({ extended: true })); // Parse form data

// Handle form submission
app.post('/submit', (req, res) => {
  const { name, email } = req.body; // Get form data
  console.log(`Received data: name=${name}, email=${email}`);

  // Process data here (e.g., store in database, send email)

  res.send('Form submitted successfully!'); // Send response
});

app.listen(8000, () => {
  console.log('Server listening on port 8000');
});
